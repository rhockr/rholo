import axios from 'axios';
import { ILink } from '../components/Links';

export interface IProfile {
  name: string,
  nickname: string,
  description?: string,
  avatar: string
  title: string,
  links: Array<ILink>
}

const { DEV } = import.meta.env
const local = 'http://localhost:3001'
const prod = 'https://express-journey.vercel.app'

export const apiInstance = axios.create({
  baseURL: DEV ? local : prod
})

const getProfile = async (): Promise<any> => {
  try {
    const request = await apiInstance.get('api/portfolio')
    return request.data
  } catch (error) {
    return 'error'
  }
}
export default getProfile
