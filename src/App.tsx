import { useQuery } from 'react-query';
import Links from './components/Links'
import Main from './components/styled/Main'
import Resume from './components/Resume'
import getProfile from './api/portfolioAPI';
import Profile from './components/Profile';

function App() {
  const { isLoading, error, data } = useQuery('portfolioData', getProfile)

  if (isLoading || error) {
    return (<div>💩</div>)
  }
  return (
    <Main>
      <Profile {...data} />
      <Links links={data.links} />
      <Resume />
    </Main>
  )
}

export default App
