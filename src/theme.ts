import { extendTheme, type ThemeConfig } from "@chakra-ui/react"

const config: ThemeConfig = {
  initialColorMode: 'dark',
  useSystemColorMode: false
}
const theme = extendTheme({
  config,
  components: {
    Link: {
      variants: {
        'like-btn': {
          bg: 'transparent',
          border: '1px solid Fuchsia'
        }
      }
    }
  }
})

export default theme

