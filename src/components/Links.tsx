import { Link, Flex } from '@chakra-ui/react'


export type ILink = {
  url: string,
  title: string
}
export interface ILinks {
  links: Array<ILink>
}
const Links = (props: ILinks) => {
  if (props.links.length === 0) {
    return (<strong>...</strong>)
  }
  return (
    <Flex mt="4">
      {
        props.links.map((link, index) => (
          <Link key={index} href={link.url} m="2" isExternal>{link.title}</Link>
        ))
      }
    </Flex>
  )
}
export default Links;
