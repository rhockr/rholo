import { Heading, Flex } from '@chakra-ui/react'
import InstAvatar from './styled/InstAvatar';
import { IProfile } from '../api/portfolioAPI';

const Profile = (props: IProfile) => {
  return (
    <Flex as="header" alignItems="center" justifyContent="center" flexDirection="column">
      <InstAvatar src={props.avatar} altText={props.nickname} size="200px" />
      <Heading
        as="h1"
        color="white"
        size="xl"
        mb="2"
        fontWeight="200">{props.name} | @{props.nickname}</Heading>
      <Heading as="h2" fontWeight="400" color="white" size="md">🤓 {props.title} 💅</Heading>
    </Flex>
  )
}

export default Profile
