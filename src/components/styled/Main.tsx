import { Flex } from '@chakra-ui/react';

type Props = {
  children?: JSX.Element[]
}
const Main = ({ children }: Props) => <Flex
  as="main"
  width="100%"
  bg="black"
  height="100vh"
  flexDir="column"
  alignItems="center"
  justifyContent="center">
  {children}
</Flex>

export default Main
