import styled from 'styled-components'
import { layout, space, SpaceProps, LayoutProps } from 'styled-system'

const CircleAvatar = styled.div<LayoutProps & SpaceProps>`
  ${layout}
  ${space}
  border-radius: 100%;
  overflow:hidden;
  display:flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  background-image: linear-gradient(
		45deg,
		#ffa95f 5%,
		#f99c4a 15%,
		#f47838 30%,
		#e75157 45%,
		#d92d7a 70%,
		#cc2a92 80%,
		#c32e92 95%
	);
  > img {
    border-radius: 50%;
    transform: scale(1);
    width: 100%;
    height: auto;
    overflow: hidden;
    border: 5px solid white;
  }
`
interface IAvatar {
  src: string,
  altText: string,
  size: string | number
}
const InstAvatar = (props: IAvatar) => {

  return (<CircleAvatar width={props.size} height={props.size} p="5px" mb="20px">
    <img src={props.src} alt={props.altText} />
  </CircleAvatar>)
}

export default InstAvatar
