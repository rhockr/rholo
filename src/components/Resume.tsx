import { Link } from '@chakra-ui/react'

const Resume = () => {
  return (<Link
    href="//rholo.cl/assets/RAhumadaCV2022.pdf"
    variant="like-btn"
    py="2"
    px="4"
    mt="4"
    borderRadius="5"
    isExternal
    download>
    Resume
  </Link>)
}

export default Resume
